%if 0%{?amzn} >= 2 || 0%{?suse_version} >= 1315
%global python3_pkgversion 3
%endif

# Currently, we build only Python 3 version, Python 2 version is no longer built
# for RHEL 7, ok to throw in Python 2 version, but not for Amazon Linux 2
%if 0%{?rhel} == 7 && 0%{?amzn} == 0
%bcond_without python2
%else
%bcond_with python2
%endif
%bcond_without python3


Name:           pip-safe
Version:        0.0.12
Release:        2%{?dist}
Summary:        Safe pip installer for command-line Python apps
License:        BSD
URL:            https://github.com/dvershinin/pip-safe
Source0:        %{url}/archive/v%{version}.tar.gz

BuildArch:      noarch

%if %{with python2}
BuildRequires:  python2-devel
BuildRequires:  python2-virtualenv
BuildRequires:  python2-six
BuildRequires:  python2-tabulate
# For tests
BuildRequires:  python2-pytest
%endif

%if %{with python3}
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
# For tests
BuildRequires:  python%{python3_pkgversion}-pytest
%endif

# CLI app depends on python3 if available, otherwise python 2
%if %{with python3}
Requires:       pip%{python3_pkgversion}-safe = %{version}-%{release}
%else
Requires:       pip2-safe = %{version}-%{release}
%endif

# if not amazon
%if 0%{?amzn} == 0
# For generation of man page
BuildRequires:  pandoc
%endif

%description
Safe and easy pip package manager for command-line Python apps

%if %{with python2}
%package -n     pip2-safe
Summary:        Install Python 2 apps from PyPi in a safe way
BuildArch:      noarch
Requires:       python2-virtualenv
Requires:       python2-six
Requires:       python2-tabulate
# some pip packages want to gcc compile stuff, so we give them:
Requires:       gcc
%{?python_provide:%python_provide python2-%{name}}

%description -n pip2-safe
Safe and easy pip package manager for command-line Python 2 apps
%endif


%if %{with python3}
%package -n     pip%{python3_pkgversion}-safe
Summary:        Install Python 3 apps from PyPi in a safe way
BuildArch:      noarch
Requires:       python%{python3_pkgversion}-six
Requires:       python%{python3_pkgversion}-tabulate
# some pip packages want to gcc compile stuff, so we give them:
Requires:       gcc
%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}

%description -n pip%{python3_pkgversion}-safe
Safe and easy pip package manager for command-line Python 3 apps
%endif

%prep
%setup -qn %{name}-%{version}
# add info section to README.md so it can be nicely converted to man page
sed -i '1i% %{name}(1)\n% Danila Vershinin - info@getpagespeed.com\n% July 2019\n\n' \
  README.md


%check
# mock does not have networking on by default.
# py.test -v


%build
%if %{with python2}
%py2_build
%endif

%if %{with python3}
%py3_build
%endif

%if 0%{?amzn} == 0
# generates man page
pandoc -s -t man README.md -o %{name}.1
%endif

%install
%if %{with python2}
%py2_install
%{__mv} %{buildroot}%{_bindir}/%{name} %{buildroot}%{_bindir}/pip2-safe
%endif

%if %{with python3}
%py3_install
%{__mv} %{buildroot}%{_bindir}/%{name} %{buildroot}%{_bindir}/pip3-safe
%{__ln_s} %{_bindir}/pip3-safe %{buildroot}%{_bindir}/%{name}
%endif

# Remove tests from install (not good folder)
rm -rf %{buildroot}%{python3_sitelib}/tests
rm -rf %{buildroot}%{python2_sitelib}/tests

%if 0%{?amzn} == 0
%{__install} -Dpm0644 %{name}.1 \
    $RPM_BUILD_ROOT%{_mandir}/man1/%{name}.1
%endif

mkdir -p $RPM_BUILD_ROOT/opt/%{name}


%files
# Virtually add license macro for EL6:
%{!?_licensedir:%global license %%doc}
#%%license LICENSE
%doc README.md
%if 0%{?amzn} == 0
%{_mandir}/man1/*.1*
%endif
%{_bindir}/%{name}
%attr(0755,root,root) %dir /opt/%{name}


%if %{with python2}
%files -n pip2-safe
# Virtually add license macro for EL6:
%{!?_licensedir:%global license %%doc}
#%%license LICENSE
%doc README.md
%{_bindir}/pip2-safe
%{python2_sitelib}/*
%endif


%if %{with python3}
%files -n pip%{python3_pkgversion}-safe
# Virtually add license macro for EL6:
%{!?_licensedir:%global license %%doc}
#%%license LICENSE
%doc README.md
%{_bindir}/pip3-safe
%{python3_sitelib}/*
%endif


%changelog
* Sat Nov 23 2019 Danila Vershinin <info@getpagespeed.com> 0.0.4-3
- first release
- include GCC to Requires: as some stuff (psutil) from PyPi wants to compile
