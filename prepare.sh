#!/bin/bash

AMZN=$(rpm -E 0%{?amzn})
# removes leading zeros, e.g. 07 becomes 0, but 0 stays 0
AMZN=${AMZN##+(0)}

# If Amazon 2, disable EPEL repo
if [[ "$AMZN" -eq 2 ]]; then
  sed -i 's/^enabled=1/enabled=0/' /etc/yum.repos.d/epel.repo
fi
